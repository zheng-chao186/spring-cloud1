package com.zheng.controller;

import com.zheng.pojo.Item;
import com.zheng.service.ItemService;
import com.zheng.web.utils.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

/**
 * @author 郑超
 * @create 2021/6/22
 */
@Slf4j
@RestController
public class ItemController {

    @Autowired
    private ItemService itemService;

    @Value(("${server.port}"))
    private int port;

    @GetMapping("/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable("orderId") String orderId) throws InterruptedException {
        log.info("server.port = " + port + "，orderId = " + orderId);

        List<Item> items = itemService.getItems(orderId);
        JsonResult<List<Item>> msg = JsonResult.ok(items).msg("port " + port);

        if (Math.random() < 0.9) {
            int t = new Random().nextInt(5000);
            log.info("随机延迟：" + t);
            Thread.sleep(t);
        }

        return msg;
    }

    @PostMapping("/decreaseNumbers")
    public JsonResult decreaseNumbers(@RequestBody List<Item> items) {
        itemService.decreaseNumbers(items);
        return JsonResult.ok();
    }

}
