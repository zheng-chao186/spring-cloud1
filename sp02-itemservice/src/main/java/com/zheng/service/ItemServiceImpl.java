package com.zheng.service;

import com.zheng.pojo.Item;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 郑超
 * @create 2021/6/22
 */
@Slf4j
@Service
public class ItemServiceImpl implements ItemService {

    @Override
    public List<Item> getItems(String orderId) {
        log.info("商品ID：" + orderId);

        ArrayList<Item> items = new ArrayList<>();
        items.add(new Item(1, "商品 1", 1));
        items.add(new Item(2, "商品 2", 2));
        items.add(new Item(3, "商品 3", 3));
        items.add(new Item(4, "商品 4", 4));
        items.add(new Item(5, "商品 5", 5));
        return items;
    }

    @Override
    public void decreaseNumbers(List<Item> items) {
        items.forEach(item -> log.info("减少库存：" + item));
    }
}
