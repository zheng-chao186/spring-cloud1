package com.zheng.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.zheng.pojo.User;
import com.zheng.web.utils.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 郑超
 * @create 2021/6/22
 */
@RefreshScope
@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Value("${sp.user-service.users}")
    private String userJson;

    @Override
    public User getUser(Integer id) {
        log.info("users：" + userJson);

        List<User> users = JsonUtil.from(userJson, new TypeReference<List<User>>() {
        });
        return users.stream().filter(user -> user.getId().equals(id)).findAny()
                .orElse(new User(id, "name-" + id, "pwd-" + id));
    }

    @Override
    public void addScore(Integer id, Integer score) {
        log.info("用户：" + id + "积分增加：" + score);
    }
}
