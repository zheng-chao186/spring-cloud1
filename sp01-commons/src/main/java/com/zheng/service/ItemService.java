package com.zheng.service;

import com.zheng.pojo.Item;

import java.util.List;

/**
 * @author 郑超
 * @create 2021/6/22
 */
public interface ItemService {

    List<Item> getItems(String orderId);

    void decreaseNumbers(List<Item> items);

}
