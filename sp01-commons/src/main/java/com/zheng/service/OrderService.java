package com.zheng.service;

import com.zheng.pojo.Order;

/**
 * @author 郑超
 * @create 2021/6/22
 */
public interface OrderService {

    Order getOrder(String orderId);

    void addOrder(Order order);

}
