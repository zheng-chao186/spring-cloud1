package com.zheng.service;

import com.zheng.pojo.User;

/**
 * @author 郑超
 * @create 2021/6/22
 */
public interface UserService {

    User getUser(Integer id);

    void addScore(Integer id, Integer score);

}
