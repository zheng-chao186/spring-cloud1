package com.zheng.web.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * @author 郑超
 * @create 2021/6/22
 */
public class CookieUtil {

    public static void setCookie(HttpServletResponse resp, String name, String value, String domain, String path, int maxAge) {
        Cookie cookie = new Cookie(name, value);
        if (domain != null)
            cookie.setDomain(domain);
        cookie.setPath(path);
        cookie.setMaxAge(maxAge);
        resp.addCookie(cookie);
    }

    public static void setCookie(HttpServletResponse resp, String name, String value, int maxAge) {
        setCookie(resp, name, value, null, "/", maxAge);
    }

    public static void setCookie(HttpServletResponse resp, String name, String value) {
        setCookie(resp, name, value, null, "/", 3600);
    }

    public static void setCookie(HttpServletResponse resp, String name) {
        setCookie(resp, name, "", null, "/", 3600);
    }

    public static String getCookie(HttpServletRequest req, String name) {
//        return Stream.of(req.getCookies()).filter(cookie -> cookie.getName().equals(name)).map(Cookie::getValue).reduce("", String::concat);

        String value = null;
        Cookie[] cookies = req.getCookies();
        if (null != cookies) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) {
                    value = cookie.getValue();
                }
            }
        }

        return value;
    }

    public static void removeCookie(HttpServletResponse response, String name, String domain, String path) {
        setCookie(response, name, "", domain, path, 0);
    }

}
