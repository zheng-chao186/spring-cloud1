package com.zheng.fallback;

import com.zheng.web.utils.JsonResult;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

/**
 * hystrix降级
 *
 * @author 郑超
 * @create 2021/6/25
 */
@Component
public class ItemFallBack implements FallbackProvider {

    // 针对那个服务应用当前降级类
    // 返回服务id：
    //      item-service - 只针对商品服务降级
    //      *       - 对所有服务降级
    //      null    - 对所有服务降级
    @Override
    public String getRoute() {
        return "item-service";
    }

    // 返回的降级响应，封装在 response 对象中
    // 根据自己的业务需求，可以返回任意的响应数据
    //          - 错误提示
    //          - 缓存数据
    //          - 执行业务运算的返回结果
    @Override
    public ClientHttpResponse fallbackResponse(String route, Throwable cause) {
        return new ClientHttpResponse() {
            @Override
            public HttpStatus getStatusCode() {
                return HttpStatus.INTERNAL_SERVER_ERROR;
            }

            @Override
            public int getRawStatusCode() {
                return HttpStatus.INTERNAL_SERVER_ERROR.value();
            }

            @Override
            public String getStatusText() {
                return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
            }

            @Override
            public void close() {
                // BAIS 流是内存数组流，不占用底层系统资源
            }

            @Override
            public InputStream getBody() throws UnsupportedEncodingException {
                String json = JsonResult.err().code(500).msg("测试hystrix降级返回").toString();
                return new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8));
            }

            @Override
            public HttpHeaders getHeaders() {
                HttpHeaders headers = new HttpHeaders();
                headers.add("Content-Type", "application/json;charset=UTF-8");
                return headers;
            }
        };
    }
}
