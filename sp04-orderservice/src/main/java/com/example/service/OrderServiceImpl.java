package com.example.service;

import com.example.feign.ItemClient;
import com.example.feign.UserClient;
import com.zheng.pojo.Item;
import com.zheng.pojo.Order;
import com.zheng.pojo.User;
import com.zheng.service.OrderService;
import com.zheng.web.utils.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 郑超
 * @create 2021/6/22
 */
@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private ItemClient itemClient;

    @Autowired
    private UserClient userClient;

    @Override
    public Order getOrder(String orderId) {
        JsonResult<List<Item>> items = itemClient.getItems(orderId);
        JsonResult<User> user = userClient.getUser(8);

        Order order = new Order();
        order.setId(orderId);
        order.setUser(user.getData());
        order.setItems(items.getData());
        return order;
    }

    @Override
    public void addOrder(Order order) {
        log.info("保存订单：" + order);
        itemClient.decreaseNumbers(order.getItems());
        userClient.addScore(order.getUser().getId(), 1000);
    }
}
