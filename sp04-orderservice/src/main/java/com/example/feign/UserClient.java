package com.example.feign;

import com.zheng.pojo.User;
import com.zheng.web.utils.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author 郑超
 * @create 2021/6/23
 */
@FeignClient(name = "user-service")
public interface UserClient {

    @GetMapping("/{userId}")
    JsonResult<User> getUser(@PathVariable Integer userId);

    @GetMapping("/{userId}/{score}")
    JsonResult<?> addScore(@PathVariable Integer userId, @PathVariable Integer score);
}
