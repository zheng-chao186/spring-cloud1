package com.example.feign;

import com.zheng.pojo.Item;
import com.zheng.web.utils.JsonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author 郑超
 * @create 2021/6/23
 */
@FeignClient(name = "item-service")
public interface ItemClient {

    @GetMapping("/{orderId}")
    JsonResult<List<Item>> getItems(@PathVariable String orderId);

    @PostMapping("/decreaseNumbers")
    JsonResult<?> decreaseNumbers(@RequestBody List<Item> items);
}
